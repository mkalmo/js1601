(function () {
    'use strict';

    angular.module('app').config(Conf);

    function Conf($routeProvider) {

        $routeProvider.when('/list', {
            templateUrl : '...',
            controller : '...',
            controllerAs : 'vm'
        }).when('/new', {
            templateUrl : '...',
            controller : '...',
            controllerAs : 'vm'
        }).otherwise('/list');
    }

})();