var app = angular.module('app', ['ngRoute']);

angular.element(document).ready(function() {
    angular.bootstrap(document, ['app']);
});

app.constant('baseUrl', window.location.origin);

app.config(['$locationProvider', function($locationProvider) {
    $locationProvider.hashPrefix('');
}]);
