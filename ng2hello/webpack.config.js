var webpack = require('webpack');
var path = require("path");

module.exports = {

    entry: {
        polyfills: './app/polyfills.ts',
        app: './app/main.ts'
    },

    output: {
        path: path.resolve(__dirname, "build"),
        publicPath: "/assets/",
        filename: '[name].js'
    },


    module: {
        loaders: [
            {
                test: /\.ts$/,
                loader: 'awesome-typescript-loader'
            }
        ]
    },

    resolve: {
        extensions: ['', '.js', '.ts']
    },

    devtool: "#inline-source-map",

    devServer: {
        proxy: {
            '/api': {
                target: 'http://localhost:3000',
                secure: false
            }
        }
    }

};
